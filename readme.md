# Guess Game


## Code Structure
* Application is based on Rest Api implemented in Spring Boot with maven
* Reactjs is used to create User Interface
* For simplicity react minimized code is kept embeded inside spring boot project only


## Steps to run
* clone the project
* run with maven (mvn spring-boot:run)
	* Optional: Jar file is also included with project project can be run with(java -jar [jarfilename.jar])	 


## Implementations and Usage
* After server is up user need to navigate to browser window and hit localhost:8080/index.html
* It will display game page
* You are allowed to guess 3 times
* After 3 incorrect attempts it shows Game over. User can start new Game
* Whenever correct guess entered by user it print correct answer and User can start new Game




[React Source Code](https://bitbucket.org/utsavmori/factset-react/src/master/)
