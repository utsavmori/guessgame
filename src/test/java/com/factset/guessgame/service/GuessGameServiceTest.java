package com.factset.guessgame.service;

import com.factset.guessgame.dto.Correctness;
import com.factset.guessgame.dto.GuessObject;
import com.factset.guessgame.exception.InvalidInputException;
import org.junit.Assert;
import org.junit.Test;


public class GuessGameServiceTest {

    @Test
    public void verifyValuesOfGuessObject() throws Exception {
        GuessVerification guessVerification = new GuessVerification();
        RandomNumber.resetCount();
        GuessObject guessObject1 = guessVerification.verifyGuess("5");
        Correctness result;
        Boolean gameOver = false;
        switch (Math.abs(guessObject1.getNumber() - RandomNumber.getNumber())) {
            case 0:
                result = Correctness.CORRECT;
                gameOver = true;
                break;
            case 1:
                result = Correctness.HOT;
                break;
            case 2:
                result = Correctness.WARM;
                break;
            default:
                result = Correctness.COLD;
        }
        Assert.assertEquals(result, guessObject1.getCorrectness());
        Assert.assertEquals(gameOver, guessObject1.getGameTerminated());
        Assert.assertEquals((Integer) 5, guessObject1.getNumber());
    }


    @Test
    public void verifyCountAndGameOver() throws Exception {
        GuessVerification guessVerification = new GuessVerification();
        RandomNumber.resetCount();
        Boolean previouslyTerminatedBeforeCountReachToThree = false;
        GuessObject guessObjectOne = guessVerification.verifyGuess("5");
        if (RandomNumber.getNumber() == 5) {
            Assert.assertTrue(guessObjectOne.getGameTerminated());
            previouslyTerminatedBeforeCountReachToThree = true;
        }

        GuessObject guessObjectTwo = guessVerification.verifyGuess("6");
        if (RandomNumber.getNumber() == 6) {

            Assert.assertTrue(guessObjectTwo.getGameTerminated());
            previouslyTerminatedBeforeCountReachToThree = true;
        }

        GuessObject guessObjectThree = guessVerification.verifyGuess("7");
        if (previouslyTerminatedBeforeCountReachToThree && RandomNumber.getNumber() != 7) {
            Assert.assertFalse(guessObjectThree.getGameTerminated());
        } else {
            Assert.assertTrue(guessObjectThree.getGameTerminated());
        }

    }

    @Test(expected = InvalidInputException.class)
    public void verifyMaxBoundryValue() throws InvalidInputException {
        GuessVerification guessVerification = new GuessVerification();
        GuessObject guessObjectOne = guessVerification.verifyGuess("11");
    }

    @Test
    public void verifyMinBoundryValue() {
        GuessVerification guessVerification = new GuessVerification();

        try {
            guessVerification.verifyGuess("0");
            Assert.assertTrue(false);
        } catch (InvalidInputException e) {
            Assert.assertTrue(true);
        }
    }

    @Test
    public void verifyNonNumber() {
        GuessVerification guessVerification = new GuessVerification();

        try {
            guessVerification.verifyGuess("factset");
            Assert.assertTrue(false);
        } catch (InvalidInputException e) {
            Assert.assertTrue(true);
        }

    }

}
