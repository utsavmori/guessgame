package com.factset.guessgame.controller;

import com.factset.guessgame.dto.GuessObject;
import com.factset.guessgame.exception.InvalidInputException;
import com.factset.guessgame.service.GuessVerification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/guessgame")
@CrossOrigin(origins = "http://localhost:3000")
public class GuessController {
    @Autowired
    GuessVerification guessVerification;

    static Logger logger = LoggerFactory.getLogger(GuessController.class);

    @GetMapping("/verify/{number}")
    public ResponseEntity<GuessObject> guessVerify(@PathVariable("number") String number) throws InvalidInputException {
        logger.info("Request received GET /guessgame/verify/{}", number);
        return new ResponseEntity<>(guessVerification.verifyGuess(number), HttpStatus.OK);

    }
}
