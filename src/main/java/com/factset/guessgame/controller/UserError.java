package com.factset.guessgame.controller;

import com.factset.guessgame.exception.InvalidInputException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class UserError {

    @ExceptionHandler({InvalidInputException.class})
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public
    ResponseEntity handleInvalidInput(final InvalidInputException exception) {

        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
}
