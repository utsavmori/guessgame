package com.factset.guessgame.service;

import java.util.Random;

public class RandomNumber {
   private static Integer number;
    private static Integer count=0;

    //private constructor to not allow instance of class
    private RandomNumber(){

    }

    static void generate(){
        number=new Random().nextInt(((10 - 1) + 1) + 1);
    }

    static Integer getNumber(){
        return  number;
    }

    static void increaseCount(){
        count++;
    }

    static Integer getCount(){
        return count;

    }

    static void resetCount(){
        count=0;
    }
}
