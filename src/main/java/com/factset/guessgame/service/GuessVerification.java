package com.factset.guessgame.service;

import com.factset.guessgame.dto.Correctness;
import com.factset.guessgame.dto.GuessObject;
import com.factset.guessgame.exception.InvalidInputException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class GuessVerification {

    static Logger logger = LoggerFactory.getLogger(GuessVerification.class);
    private Boolean gameTerminated = false;

    public GuessObject verifyGuess(String numberToGuess) throws InvalidInputException {
        Correctness correctness;

        Integer number = getIntegerOfNumber(numberToGuess);

        if (number >= 1 && number <= 10) {
            guessCountCheck();
            correctness = applyRules(number);
        } else {

            throw new InvalidInputException("Required Integer Number Must be between 1 to 10 inclusive");
        }
        return new GuessObject(number, correctness, gameTerminated);


    }

    private Integer getIntegerOfNumber(String numberToGuess) throws InvalidInputException {
        Integer number;
        try {
            number = Integer.valueOf(numberToGuess);
        } catch (NumberFormatException e) {
            throw new InvalidInputException("Required Integer Number Must be between 1 to 10 inclusive");
        }
        return number;
    }

    private void guessCountCheck() {
        if (RandomNumber.getCount() == 0) {
            setGameNotTerminated();
            RandomNumber.generate();
            RandomNumber.increaseCount();
        } else if (RandomNumber.getCount() == 2) {
            terminateGame();
        } else {
            RandomNumber.increaseCount();
        }
    }

    private Correctness applyRules(Integer number) {
        Correctness result;
        switch (Math.abs(number - RandomNumber.getNumber())) {
            case 0:
                result = Correctness.CORRECT;
                terminateGame();
                break;
            case 1:
                result = Correctness.HOT;
                break;
            case 2:
                result = Correctness.WARM;
                break;
            default:
                result = Correctness.COLD;

        }
        logger.info("Required {} Found {}", RandomNumber.getNumber(), number);
        return result;
    }

    private void terminateGame() {
        resetCount();
        setGameTerminated();
    }

    private void resetCount() {
        RandomNumber.resetCount();
    }


    private void setGameTerminated() {
        gameTerminated = true;
    }

    private void setGameNotTerminated() {
        gameTerminated = false;
    }
}
