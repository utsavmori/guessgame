package com.factset.guessgame.dto;

public class GuessObject {
    private Integer number;
    private Correctness correctness;
    private Boolean gameTerminated;


    public GuessObject() {
    }

    public GuessObject(Integer number, Correctness correctness, Boolean
            gameTerminated) {
        this.correctness = correctness;
        this.number = number;
        this.gameTerminated = gameTerminated;
    }

    public Correctness getCorrectness() {
        return correctness;
    }

    public void setCorrectness(Correctness correctness) {
        this.correctness = correctness;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Boolean getGameTerminated() {
        return gameTerminated;
    }

    public void setGameTerminated(Boolean gameTerminated) {
        this.gameTerminated = gameTerminated;
    }
}
