package com.factset.guessgame.dto;

public enum Correctness {
    COLD, WARM, HOT, CORRECT
}
